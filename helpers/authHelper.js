const jwt = require('jsonwebtoken');
const uuid = require('uuid/v4');
const { secret, tokens } = require('../config/app').jwt;
const mongoose = require('mongoose');

// Token model
const Token = require('../models/Token');

const generateAccessToken = userId => {
  const payload = {
    userId,
    type: tokens.access.type
  };
  const opts = { expiresIn: tokens.access.expiresIn };

  return jwt.sign(payload, secret, opts);
};

const generateRefreshToken = () => {
  const payload = {
    id: uuid(),
    type: tokens.refresh.type
  };

  const opts = { expiresIn: tokens.refresh.expiresIn };

  return {
    id: payload.id,
    token: jwt.sign(payload, secret, opts)
  };
};

const replaceDbRefreshToken = (tokenId, userId) =>
  Token.findOneAndRemove({ userId })
    .exec()
    .then(() => Token.create({ tokenId, userId }));

const updateTokens = userId => {
  const accessToken = generateAccessToken(userId);
  const refreshToken = generateRefreshToken();

  return replaceDbRefreshToken(refreshToken.id, userId).then(() => ({
    accessToken,
    refreshToken: refreshToken.token
  }));
};

module.exports = {
  generateAccessToken,
  generateRefreshToken,
  replaceDbRefreshToken,
  updateTokens
};
