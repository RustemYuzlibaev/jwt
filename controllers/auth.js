const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { updateTokens } = require('../helpers/authHelper');
const { secret } = require('../config/app').jwt;

// Load models
const User = require('../models/User');

// Register controller
const register = (req, res) => {
  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      return res.status(400).json({ msg: 'User already exists' });
    }

    const newUser = new User({
      email: req.body.email,
      password: req.body.password
    });

    // Password encryption
    bcrypt
      .hash(newUser.password, 10)
      .then(hash => {
        newUser.password = hash;
        newUser
          .save()
          .then(user => res.json({ msg: 'User created' }))
          .catch(err => console.log(err));
      })
      .catch(err => console.log(err));
  });
};

// Login controller
const login = async (req, res) => {
  const { email } = req.body;
  const { password } = req.body;

  const user = await User.findOne({ email });

  if (!user) {
    return res.status(404).json({ msg: 'User not found' });
  }

  bcrypt.compare(password, user.password).then(isMatch => {
    if (isMatch) {
      // User matched
      // const payload = { id: user.id, email: user.email };
      // jwt.sign(payload, secret, (err, token) => {
      //   if (err) {
      //     return res.status(500).json({ msg: 'Could not give token' });
      //   }
      //   res.json({
      //     token: 'Bearer ' + token
      //   });
      // });
      updateTokens(user._id).then(tokens => res.json(tokens));
    } else {
      return res.status(401).json({ msg: 'Password incorrect' });
    }
  });
};

// RefreshToken controller
const refreshTokens = (req, res) => {
  const { refreshToken } = req.body;
  let payload;
  try {
    payload = jwt.verify(refreshToken, secret);
    console.log(payload.type);
    if (payload.type !== 'refresh') {
      return res.status(400).json({ msg: 'Invalid token WTF' });
    }
  } catch (e) {
    if (e instanceof jwt.TokenExpiredError) {
      return res.status(400).json({ message: 'Token expired' });
    } else if (e instanceof jwt.JsonWebTokenError) {
      return res.status(400).json({ message: 'Invalid token WTF1' });
    }
  }

  Token.findOne({ tokenId: payload.id })
    .exec()
    .then(token => {
      if (token === null) {
        throw new Error('Invalid token');
      }

      return updateTokens(token.userId);
    })
    .then(tokens => res.json(tokens))
    .catch(err => res.status(400).json({ msg: err.message }));
};

module.exports = {
  register,
  login,
  refreshTokens
};
