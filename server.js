const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config/app');
const protectedRoute = require('./middleware/protectedRoute');
const app = express();

// port
app.set('port', process.env.PORT || config.port);

// body-parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// DB config
const db = config.mongoURI;

// Connect to MongoDB
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log('Connected to DB'))
  .catch(err => console.log(err));

// use routes
require('./routes/routes')(app);

app.listen(app.get('port'), () => {
  console.log(`Now listening on port ${app.get('port')}`);
});
