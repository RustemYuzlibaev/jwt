const mongoose = require('mongoose');

const TokenSchema = new mongoose.Schema({
  tokenId: {
    type: String,
    required: true
  },
  userId: {
    type: String,
    required: true
  }
});

module.exports = Token = mongoose.model('Token', TokenSchema);
