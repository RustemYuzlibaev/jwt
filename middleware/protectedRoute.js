const jwt = require('jsonwebtoken');
const { secret } = require('../config/app').jwt;

module.exports = (req, res, next) => {
  // const authHeader = req.headers['authorization']
  const authHeader = req.get('Authorization');
  if (!authHeader) {
    res.status(401).json({ msg: 'Token not provided' });
  }

  const token = authHeader.replace('Bearer ', '');

  try {
    const payload = jwt.verify(token, secret);
    if (payload.type !== 'access') {
      return res.status(401).json({ msg: 'Invalid token' });
    }
  } catch (e) {
    if (e instanceof jwt.TokenExpiredError) {
      return res.status(401).json({ msg: 'Token expired' });
    }

    if (e instanceof jwt.JsonWebTokenError) {
      return res.status(401).json({ msg: 'Invalid token' });
    }
  }

  next();
};
