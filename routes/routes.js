const { register } = require('../controllers/auth');
const { login } = require('../controllers/auth');
const { refreshTokens } = require('../controllers/auth');
const { private } = require('../controllers/private');
const { public } = require('../controllers/public');
const protectedRoute = require('../middleware/protectedRoute');

module.exports = app => {
  // @route POST api/users/register
  // @desc Register user
  // @access Public
  app.post('/register', register);

  // @route POST api/users/login
  // @desc Login user
  // @access Public
  app.post('/login', login);

  // @route POST api/users/refresh-tokens
  // @desc Refresh token
  // @access Public
  app.post('/refresh-tokens', refreshTokens);

  // @route GET /private
  // @desc Homeapage
  // @access Private
  app.get('/private', protectedRoute, private);

  // @route GET /public
  // @desc Homeapage
  // @access Public
  app.get('/public', public);
};
