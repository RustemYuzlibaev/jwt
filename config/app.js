module.exports = {
  port: 3000,
  mongoURI:
    'mongodb://jsonwebtoken:pass1234@ds157204.mlab.com:57204/jsonwebtoken',
  jwt: {
    secret: 'secret',
    tokens: {
      access: {
        type: 'access',
        expiresIn: '2m'
      },
      refresh: {
        type: 'refresh',
        expiresIn: '3m'
      }
    }
  }
};
